use qmetaobject::{
    connect,
    connections::{ConnectionHandle, Slot},
    QObject, QObjectPinned, Signal,
};
use std::cell::RefCell;

/// This is purely internal and should only be used through the connect! macro.
#[doc(hidden)]
pub fn internal_safe_connect<T: QObject + Sized, Args, F: Slot<Args>>(
    sender: &RefCell<T>,
    signal: Signal<Args>,
    slot: F,
) -> ConnectionHandle {
    let sender = unsafe { QObjectPinned::new(&sender).get_or_create_cpp_object() };

    unsafe { connect(sender, signal, slot) }
}

///
/// Connects a signal to a slot. The slot will be invoked when the signal is emitted
///
/// ```
/// connect!(obj, url_changed, (|| {
///     obj.borrow_mut().fetch_data();
/// });
/// ```
///
#[macro_export]
macro_rules! connect {
    ($sender:ident, $signal:ident, $slot:expr) => {
        {
            let signal = $sender
                .borrow()
                .$signal
                .to_cpp_representation(&*$sender.borrow());

            #[allow(unused_parens)]
            crate::helpers::internal_safe_connect(&$sender, signal, $slot)
        }
    };
}

#[test]
fn connect_macro() {
    use qmetaobject::*;

    #[derive(QObject, Default)]
    struct Foo {
        base: qt_base_class!(trait QObject),
        my_signal: qt_signal!(),
        my_signal2: qt_signal!(x: i32, y: String),
    }

    let f = RefCell::new(Foo::default());
    let mut result: Option<String> = None;
    let con = connect!(f, my_signal, || {
        result = Some("moin".to_owned());
    });

    assert!(con.is_valid());

    f.borrow().my_signal();
    assert_eq!(result, Some("moin".to_owned()));

    let con2 = connect!(f, my_signal2, |x: &i32, y: &String| -> () {
        result = Some(format!("{} -> {}", x, y));
    });

    assert!(con2.is_valid());
    f.borrow().my_signal2(1, "2".to_owned());

    assert_eq!(result, Some("1 -> 2".to_owned()));
}
