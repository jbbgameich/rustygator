extern crate qmetaobject;
extern crate atom_syndication;
use qmetaobject::{QObject, USER_ROLE, lazy_static, qmetaobject_lazy_static, qt_base_class, qt_method, qt_property, qt_signal};
use qmetaobject::listmodel::{QAbstractListModel};
use qmetaobject::qttypes::{QVariantList, QUrl, QString, QByteArray, QModelIndex, QVariant};
use atom_syndication::*;
use std::{collections::HashMap};
use std::io::BufReader;
use num_traits::FromPrimitive;
use std::default::Default;
use std::cell::RefCell;

use crate::connect;

#[derive(FromPrimitive, ToPrimitive)]
enum Roles {
    TitleRole = USER_ROLE as isize + 1,
    IdRole,
    UpdatedRole,
    AuthorsRole,
    CategoriesRole,
    ContributorsRole,
    LinksRole,
    PublishedRole,
    RightsRole,
    SourceRole,
    SummaryRole,
    ContentRole
}

#[derive(QObject)]
pub struct AtomModel {
    base: qt_base_class!(trait QAbstractListModel),
    list: Vec<Entry>,

    // methods
    fetch_data: qt_method!(fn(&mut self)),

    // properties
    url: qt_property!(String; NOTIFY url_changed),
    title: qt_property!(String; NOTIFY title_changed),
    subtitle: qt_property!(String; NOTIFY subtitle_changed),
    updated: qt_property!(String; NOTIFY updated_changed),
    generator: qt_property!(String; NOTIFY generator_changed),
    icon: qt_property!(QUrl; NOTIFY icon_changed),
    logo: qt_property!(QUrl; NOTIFY logo_changed),
    categories: qt_property!(QVariantList; NOTIFY categories_changed),

    // signals
    data_fetched: qt_signal!(),
    url_changed: qt_signal!(),
    title_changed: qt_signal!(),
    subtitle_changed: qt_signal!(),
    updated_changed: qt_signal!(),
    generator_changed: qt_signal!(),
    icon_changed: qt_signal!(),
    logo_changed: qt_signal!(),
    categories_changed: qt_signal!()
}

impl QAbstractListModel for AtomModel {
    fn row_count(&self) -> i32 {
        return self.list.len() as i32
    }

    fn data(&self, index: QModelIndex, role: i32) -> QVariant {
        let idx = index.row() as usize;
        if idx < self.row_count() as usize {
            return match FromPrimitive::from_i32(role) {
                Some(Roles::TitleRole) => QString::from(self.list[idx].title()).into(),
                Some(Roles::IdRole) => QString::from(self.list[idx].id()).into(),
                Some(Roles::UpdatedRole) => QString::from(self.list[idx].updated().to_rfc3339()).into(),
                Some(Roles::AuthorsRole) => {
                    let mut vec = QVariantList::default();

                    for author in self.list[idx].authors() {
                        vec.push(QString::from(author.name()).into());
                    }

                    vec.into()
                },
                Some(Roles::CategoriesRole) => {
                    let mut vec = QVariantList::default();

                    for category in self.list[idx].categories() {
                        vec.push(QString::from(category.label().unwrap_or_default()).into())
                    }

                    vec.into()
                }
                Some(Roles::ContributorsRole) => {
                    let mut vec = QVariantList::default();

                    for contributor in self.list[idx].contributors() {
                        vec.push(QString::from(contributor.name()).into())
                    }

                    vec.into()
                },
                Some(Roles::LinksRole) => {
                    let mut vec = QVariantList::default();

                    for link in self.list[idx].links() {
                        vec.push(QString::from(link.href()).into())
                    }

                    vec.into()
                },
                Some(Roles::PublishedRole) => QString::from(self.list[idx].published().map(|x|x.to_rfc3339()).unwrap_or_default()).into(),
                Some(Roles::RightsRole) => QString::from(self.list[idx].rights().unwrap_or_default()).into(),
                Some(Roles::SourceRole) => QString::from(self.list[idx].source().unwrap_or(&Source::default()).title()).into(),
                Some(Roles::SummaryRole) => QVariant::from(QString::from(self.list[idx].summary().unwrap_or_default())),
                Some(Roles::ContentRole) => QVariant::from(QString::from(self.list[idx].content().unwrap_or(&Content::default()).src().unwrap_or_default())),
                _ => QVariant::default()
            };
        }

        return QVariant::default();
    }

    fn role_names(&self) -> HashMap<i32, QByteArray> {
        let mut map = HashMap::new();
        map.insert(Roles::TitleRole as i32, "title".into());
        map.insert(Roles::IdRole as i32, "id".into());
        map.insert(Roles::UpdatedRole as i32, "updated".into());
        map.insert(Roles::AuthorsRole as i32, "authors".into());
        map.insert(Roles::CategoriesRole as i32, "categories".into());
        map.insert(Roles::ContributorsRole as i32, "contributors".into());
        map.insert(Roles::LinksRole as i32, "links".into());
        map.insert(Roles::PublishedRole as i32, "published".into());
        map.insert(Roles::RightsRole as i32, "rights".into());
        map.insert(Roles::SourceRole as i32, "source".into());
        map.insert(Roles::SummaryRole as i32, "summary".into());
        map.insert(Roles::ContentRole as i32, "content".into());
        return map;
    }
}

impl AtomModel {
    fn fetch_data(&mut self) {
        let client = reqwest::blocking::Client::new();
        let request = client.get(&self.url);
        (self as &mut dyn QAbstractListModel).begin_reset_model();
        match request.send() {
            Ok(r) => {
                let feed = Feed::read_from(BufReader::new(r)).unwrap();
                self.list = feed.entries().to_vec();
                self.title = feed.title().to_string();
                self.subtitle = feed.subtitle().unwrap_or_default().to_string();
                self.updated = feed.updated().to_rfc3339();
                self.generator = feed.generator().unwrap_or(&Generator::default()).value().to_string();
                self.icon = QString::from(feed.icon().unwrap_or_default().to_string()).into();
                self.logo = QString::from(feed.logo().unwrap_or_default().to_string()).into();
                for cat in feed.categories() {
                    self.categories.push(QString::from(cat.label().unwrap_or_default()).into())
                }

                /*emit*/ self.title_changed();

                println!("Successfully fetched data");
            }
            Err(_e) => println!("Failed to fetch data")
        }
        (self as &mut dyn QAbstractListModel).end_reset_model();
    }
}

impl Default for AtomModel {
    fn default() -> Self {
        let obj = RefCell::new(Self {
            base: Default::default(),
            list: Default::default(),
            fetch_data: Default::default(),
            url: Default::default(),
            title: Default::default(),
            subtitle: Default::default(),
            updated: Default::default(),
            generator: Default::default(),
            icon: Default::default(),
            logo: Default::default(),
            categories: Default::default(),
            data_fetched: Default::default(),
            url_changed: Default::default(),
            title_changed: Default::default(),
            subtitle_changed: Default::default(),
            updated_changed: Default::default(),
            generator_changed: Default::default(),
            icon_changed: Default::default(),
            logo_changed: Default::default(),
            categories_changed: Default::default()
        });

        connect!(obj, url_changed, (|| {
            obj.borrow_mut().fetch_data();
        }));

        obj.into_inner()
    }
}
