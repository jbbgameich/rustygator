extern crate serde;

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct Feed {
    pub url: String,
    pub name: String
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Settings {
    pub feeds: Vec<Feed>
}
