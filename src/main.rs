#![feature(proc_macro_hygiene)]

extern crate reqwest;
extern crate qmetaobject;
#[macro_use] extern crate cstr;
#[macro_use] extern crate num_derive;

use qmetaobject::{qml_register_type, qrc, QString, QmlEngine};

mod atommodel;
mod settings;
mod feedsmodel;
mod helpers;

fn main() {
    qrc!(ressources,
        "qml" {
            "src/qml/main.qml" as "main.qml",
            "src/qml/EntryDetailsPage.qml" as "EntryDetailsPage.qml",
            "src/qml/DetailsItem.qml" as "DetailsItem.qml",
            "src/qml/FeedsPage.qml" as "FeedsPage.qml",
            "src/qml/FeedDetailsPage.qml" as "FeedDetailsPage.qml",
            "src/qml/AtomPage.qml" as "AtomPage.qml"
         }
    );

    ressources();

    // Register types
    qml_register_type::<atommodel::AtomModel>(cstr!("org.kde.rustygator"), 1, 0, cstr!("AtomModel"));
    qml_register_type::<feedsmodel::FeedsModel>(cstr!("org.kde.rustygator"), 1, 0, cstr!("FeedsModel"));

    // QML Engine
    let mut engine = QmlEngine::new();
    engine.add_import_path(QString::from("qrc:/qml/"));
    engine.load_file(QString::from("qrc:/qml/main.qml"));

    engine.exec();
}
