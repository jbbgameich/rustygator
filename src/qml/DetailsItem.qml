import QtQuick 2.7
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.2 as Controls

import org.kde.kirigami 2.2 as Kirigami

ColumnLayout {
    property string name
    property string content
    property alias textFormat: label.textFormat

    visible: content

    Kirigami.Heading {
        text: parent.name ? parent.name : ""
        Layout.fillWidth: true
        level: 3
    }
    Controls.Label {
        id: label
        text: parent.content ? parent.content : ""
        Layout.fillWidth: true
        wrapMode: Text.WordWrap
    }
}
