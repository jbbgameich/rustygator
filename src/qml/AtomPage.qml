import QtQuick 2.7
import QtQuick.Controls 2.10 as Controls
import org.kde.kirigami 2.2 as Kirigami

import org.kde.rustygator 1.0

Kirigami.ScrollablePage {
    id: page
    property string url
    title: atomModel.title

    contextualActions: [
        Kirigami.Action {
            text: "Details"
            onTriggered: pageStack.push("qrc:/qml/FeedDetailsPage.qml", {"modelData": atomModel})
        }
    ]

    // HACK: I'm likely doing something completely wrong in the backend
    onUrlChanged: {
        console.log("Url changed")
        atomModel.url = url
        atomModel.url_changed()
        atomModel.fetch_data()
    }

    ListView {
        model: AtomModel {
            id: atomModel
        }

        Connections {
            target: atomModel
            // onUrl_changed: {
            //     print("url changed")
            //     atomModel.fetch_data()
            // }
        }

        delegate: Kirigami.BasicListItem {
            text: model.title
            width: parent.width

            onClicked: {
                pageStack.push("qrc:/qml/EntryDetailsPage.qml", {"modelData": model})
            }
        }
    }
}
