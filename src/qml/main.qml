import QtQuick 2.7
import QtQuick.Controls 2.10 as Controls
import org.kde.kirigami 2.2 as Kirigami
import org.kde.rustygator 1.0

Kirigami.ApplicationWindow {
    title: "Feed reader"
    visible: true
    width: 400
    height: 400

    pageStack.initialPage: "qrc:/qml/FeedsPage.qml"
}
