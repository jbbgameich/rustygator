# Rustygator

A Kirigami RSS/Atom Reader written in Rust

## Building

Rustygator currently only builds on nightly versions of Rust. Nightly can be switched on using  `$ rustup override set nightly`.

Rustygator can then be built with `$ cargo build` and run with `$ cargo run`